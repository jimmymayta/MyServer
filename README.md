# MyServer
MyServer es una servidor alternativo a XAMPP o WAMP, portable facil de usar, la carpeta de directorio es 'www', que se encuentra en la carpeta raiz.

Es compatible con varias bases de datos como ser, PostgreSQL, MariaDB, SQLite

- Iniciar MyServer :Start.bat

- Detener MyServer :Stop.bat

***
### __Instalación__
### 1. PATH de windows, debe ingresar las siguientes rutas
```{r}
C:\MyServer\Apache24\bin
C:\MyServer\php-7.4.8
```
### 2. Instalar MyServer
```{r}
Ejecutar Install.Bat (Admin)
```

### 3. Iniciar MyServer
```{r}
Ejecutar Start.Bat (Admin)
```

### 4. Iniciar MyServer en el navegador
```{r}
Iniciar en el navegador: https://127.0.0.1 o https://localhost
Iniciar phpmyadmin: 127.0.01/phpmyadmin
```

### 5. Verificar la ruta que este correctamente declarada
```{r}
Direcotrio: C:\MyServer\www
Lo puede cambiar editando el siguiente archivo:

C:\MyServer\Apache24\conf\httpd.conf

38. Define Ruta "C:/MyServer/www"
```
(Lo puede cambiar a gusto, puede cambiar a distintos directorios)
***
### __Datos de MyServer__
Dominio: localhost o 127.0.01
Puerto:80

### __Datos de MyServer__
Direcotrio: C:\MyServer\www
Lo puede cambiar editando el siguiente archivo:

C:\MyServer\Apache24\conf\httpd.conf
```{r}
38. Define Ruta "C:/MyServer/www"
```
(Recuerde que tiene que copiar la carpeta de 'phpmyadmin' para que funcione el phpmyadmin)

### __Seguridad__
Si lo que desea es seguridad tiene que camibar el blowfish_secret de phpmyadmin, puede generar uno en: https://phpsolved.com/phpmyadmin-blowfish-secret-generator/

C:\MyServer\www\phpMyAdmin\config.inc.php
```{r}
17. $cfg['blowfish_secret'] = 'ni1q73cuzm0o,;PtEw/6mFAF:C0z]U8E';
```

### __Aplicaciones__
- Apache HTTP Server 2.4.43
- PHP 7.4.8
- phpMyAdmin

### __Bases de datos__
PostgreSQL: The world's most advanced open source database: www.postgresql.org

https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

MariaDB Foundation - MariaDB.org: mariadb.org

https://downloads.mariadb.org/mariadb/10.5.4/

SQLite Home Page: www.sqlite.org

https://www.sqlite.org/download.html

https://www.sqlite.org/2020/sqlite-dll-win32-x86-3320300.zip

https://www.sqlite.org/2020/sqlite-tools-win32-x86-3320300.zip


### __Links__
Apache 2.4.43
* Apache 2.4 VS16 Windows Binaries and Modules: https://www.apachelounge.com/download/
* Visual C++: https://aka.ms/vs/16/release/VC_redist.x64.exe
* Apache 2.4.43 Win64: https://www.apachelounge.com/download/VS16/binaries/httpd-2.4.43-win64-VS16.zip

PHP 7.4.8
* PHP 7.4.8: https://www.php.net/downloads
* PHP 7.4 (7.4.8): https://windows.php.net/download#php-7.4
* VC15 x64 Thread Safe (2020-Jul-09 14:57:01): https://windows.php.net/downloads/releases/php-7.4.8-Win32-vc15-x64.zip

phpMyAdmin
* phpmyadmin: https://www.phpmyadmin.net/
* phpmyadmin/downloads: https://www.phpmyadmin.net/downloads/
* phpMyAdmin-4.9.5-all-languages.zip: https://files.phpmyadmin.net/phpMyAdmin/4.9.5/phpMyAdmin-4.9.5-all-languages.zip

### __Windows: PATH__
C:\MyServer\Apache24\bin

C:\MyServer\php-7.4.8

### __Para próximas versiones__
Interfaz grafica programada en python 3

### __Desarrollador__
Jimmy Mayta

jimmyymaytaj@gmail.com










